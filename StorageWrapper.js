/**
 * A "module" that makes calls to the available storage (sync or local).
 *
 * For some reason sometimes sync storage isn't available and we have to fall back to storage.
 */
StorageWrapper = {
    _initialized: false,
    _errorDuringInit: null,
    _storage: browser.storage.sync
}

StorageWrapper.init = async () => {
    try {
        await StorageWrapper._storage.get("_")
        StorageWrapper._initialized = true
        console.debug("Using sync storage")
        return StorageWrapper
    } catch (exception) {
        // Try the local storage
        await StorageWrapper._fallbackToLocalStorage()
        return StorageWrapper
    }
}

StorageWrapper._fallbackToLocalStorage = async function () {
    try {
        StorageWrapper._storage = browser.storage.local;
        await StorageWrapper._storage.get("_")
        StorageWrapper._initialized = true
        console.debug("Using local storage")
    } catch (exception) {
        StorageWrapper._errorDuringInit = exception
        throw exception
    }
}

StorageWrapper._checkInitialized = function () {
    if (!StorageWrapper._initialized) {
        throw "Not initialized"
    }

    if (StorageWrapper._errorDuringInit) {
        throw StorageWrapper._errorDuringInit
    }
}

StorageWrapper.get = function () {
    StorageWrapper._checkInitialized()
    return StorageWrapper._storage.get.apply(StorageWrapper._storage, arguments)
}
StorageWrapper.set = function () {
    StorageWrapper._checkInitialized()
    return StorageWrapper._storage.set.apply(StorageWrapper._storage, arguments)
}
StorageWrapper.remove = function () {
    StorageWrapper._checkInitialized()
    return StorageWrapper._storage.remove.apply(StorageWrapper._storage, arguments)
}
StorageWrapper.clear = function () {
    StorageWrapper._checkInitialized()
    return StorageWrapper._storage.clear(arguments)
}
