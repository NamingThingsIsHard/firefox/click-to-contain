A mini-plugin that will force opening external links of certain pages in random containers.

# Development

You will need [yarn](https://yarnpkg.com) (an alternative to npm(node package manager))
and of course firefox

**Initializing the project**

`yarn`

**Running in dev mode**

`yarn start`

**Building for publishing**

`yarn run build`

# Thanks

 * [Antrepo](http://a2591.com) for the icon (found on [iconspedia](http://www.iconspedia.com/icon/attention-container-133-6.html))
