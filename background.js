// From https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/contextualIdentities/create
const CONTEXT_COLORS = [
    "blue",
    "turquoise",
    "green",
    "yellow",
    "orange",
    "red",
    "pink",
    "purple"
];

const CONTEXT_ICONS = [
    "fingerprint",
    "briefcase",
    "dollar",
    "cart",
    "circle"
];

const CONTEXT_PREFIX = "click_to_contain-";

/**
 * Key: tab id
 * value: context / cookie store id
 * @type {{}}
 */
const tabContexts = {};

function countTabsInContext(contextId) {
    if (!contextId) {
        throw "Must provide contextId"
    }
    return Object.keys(tabContexts).filter((tabId) => tabContexts[tabId] === contextId).length
}

function pickRandomly(array) {
    return array.length > 0 ? array[Math.floor(Math.random() * array.length)] : null
}

var contentPort;

function handleMessages({command, data}, {sender}) {
    if (command === "openTab") {
        cmdOpenTab({url: data.url, openerTabId: sender.tab ? sender.tab.id : null})
    }
}

/**
 * Opens a tab in a random context / cookie store
 * @param url {String}
 * @param openerTabId {Number=} ID of the tab which is requesting to open a new one
 * @param active {boolean} whether the tab should be active
 * @param index {number} where the tab should be placed
 * @return {Promise<Tab>}
 */
async function cmdOpenTab({url, openerTabId, active, index}) {
    const contextIdentity = await browser.contextualIdentities.create({
        name: CONTEXT_PREFIX + Date.now(),
        color: pickRandomly(CONTEXT_COLORS),
        icon: pickRandomly(CONTEXT_ICONS)
    });
    try {
        const tab = await browser.tabs.create({
            url: url,
            cookieStoreId: contextIdentity.cookieStoreId,
            openerTabId: openerTabId,
            active: active,
            index: index
        });
        tabContexts[tab.id] = contextIdentity.cookieStoreId;
        return tab
    } catch (error) {
        browser.contextualIdentities.remove(contextIdentity.cookieStoreId);
        console.error("error creating tab", error);
    }
}

async function reopenTab(tab) {
    await cmdOpenTab(tab);
    return browser.tabs.remove(tab.id);
}

function connected(p) {
    contentPort = p;
    contentPort.onMessage.addListener(handleMessages);
}

browser.runtime.onConnect.addListener(connected);

// Keep track of the tabs in the contexts we own
browser.tabs.onCreated.addListener((tab) => {
    let tabContextId = tab.cookieStoreId;
    let contextIds = Object.values(tabContexts);
    if (contextIds.includes(tabContextId)) {
        tabContexts[tab.id] = tabContextId;
    }
})
// Remove the temporary context when the last tab is removed
browser.tabs.onRemoved.addListener((tabId) => {
    let tabContextId = tabContexts[tabId];
    if (tabContextId) {
        delete tabContexts[tabId];
        // Prefer tracking tabs and their contexts to
        // calling browser.tabs.query with the contextId
        // It's lighter and faster
        if (countTabsInContext(tabContextId) < 1) {
            console.info("Removing empty container. ID:", tabContextId);
            browser.contextualIdentities.remove(tabContextId)
        }
    }
})

const MENU_OPEN_LINK = "open-link";
const MENU_OPEN_TAB = "open-tab";
const MENU_REOPEN_TAB = "reopen-tab";
const CONTEXT_MENUS = {
    [MENU_OPEN_LINK]: {
        createInfo: {
            title: "Open in random container",
            contexts: [
                "image",
                "link",
            ]
        },
        onClicked: (info, tab) => {
            const url = info.srcUrl || info.linkUrl;
            cmdOpenTab({url, openerTabId: tab.id});
        }
    },
    [MENU_OPEN_TAB]: {
        createInfo: {
            title: "Open a new random container",
            contexts: [
                "tab",
                "page",
            ]
        },
        onClicked: (info, tab) => {
            cmdOpenTab({openerTabId: tab.id});
        }
    },
    [MENU_REOPEN_TAB]: {
        createInfo: {
            title: "Reopen in a random container",
            contexts: [
                "tab",
                "page",
            ]
        },
        onClicked: (info, tab) => {
            reopenTab(tab);
        }
    },
};

for (let contextmenuId in CONTEXT_MENUS) {
    const contextMenu = Object.assign(
        {id: contextmenuId},
        CONTEXT_MENUS[contextmenuId].createInfo);
    console.log("creating contextmenu", contextMenu);
    browser.menus.create(contextMenu);
}

function onMenuClick(info, tab) {
    const contextMenu = CONTEXT_MENUS[info.menuItemId];
    if (!contextMenu) {
        return
    }
    console.log("onClicked", info, tab)
    contextMenu.onClicked(info, tab);
}

browser.menus.onClicked.addListener(onMenuClick);

async function onCommand(command) {
    const currentTab = (await browser.tabs.query({
        active: true,
        currentWindow: true
    }))[0];
    if (command === MENU_OPEN_TAB) {
        return cmdOpenTab({openerTabId: currentTab.id});
    } else if (command === MENU_REOPEN_TAB) {
        return reopenTab(currentTab);
    }
}

browser.commands.onCommand.addListener(onCommand);

browser.contextualIdentities.onRemoved.addListener(({contextualIdentity}) => {
    StorageWrapper.get("settings").then((settings) => {
        if (settings && settings.notifyOnRemove) {
            browser.notifications.create({
                type: "basic",
                title: "Container deleted",
                message: contextualIdentity.name
            })
        }
    }).catch((reason) => {
        console.warn("Couldn't retrieve settings --> not testing for notification", reason);
    })
})

// Clean up left over containers
browser.contextualIdentities.query({}).then((contexts) => {
    contexts.filter((context) => {
        return context.name.startsWith(CONTEXT_PREFIX)
    }).forEach((context) => {
        console.info("Removing context: ", context.name)
        browser.contextualIdentities.remove(context.cookieStoreId)
    })
})

browser.runtime.onConnectExternal.addListener((port) => {
    console.log(`External connection from ${port.sender.id}`)
    port.onMessage.addListener(handleMessages)
})
StorageWrapper.init()
